#!/usr/bin/env python
#
# Copyright (C) 2005-2010 TUBITAK/UEKAE
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# Please read the COPYING file.
import os
import glob
import shutil
import sipconfig
from distutils.core import setup, Extension
from distutils.sysconfig import get_python_lib
from distutils.cmd import Command
from distutils.command.build import build
from distutils.command.clean import clean
from distutils.command.install import install
from distutils.spawn import find_executable, spawn

I18N_DOMAIN = "sulins"
I18N_LANGUAGES = ["tr", "nl", "it", "fr", "de", "pt_BR", "es", "pl", "ca", "sv", "hu", "ru", "hr"]

def qt_ui_files():
    ui_files = "sulins/gui/Ui/*.ui"
    return glob.glob(ui_files)

def py_file_name(ui_file):
    return os.path.splitext(ui_file)[0] + '.py'

class SulinsBuild(build):
    def changeQRCPath(self, ui_file):
        py_file = py_file_name(ui_file)
        lines = open(py_file, "r").readlines()
        replaced = open(py_file, "w")
        for line in lines:
            if line.find("data_rc") != -1:
                continue
            replaced.write(line)

    def compileUI(self, ui_file):
        pyqt_configuration = sipconfig.Configuration()
        pyuic_exe = find_executable('pyuic5', pyqt_configuration.default_bin_dir)
        if not pyuic_exe:
            pyuic_exe = find_executable('pyuic5')

        cmd = [str(pyuic_exe), str(ui_file), '-o']
        cmd.append(str(py_file_name(ui_file)))
        #cmd.append("-g \"sulins\"")
        os.system(' '.join(cmd))

    def run(self):
        for ui_file in qt_ui_files():
            print(ui_file)
            #self.compileUI(ui_file)
            #self.changeQRCPath(ui_file)
        build.run(self)

class SulinsClean(clean):

    def run(self):
        clean.run(self)

        for ui_file in qt_ui_files():
            ui_file = py_file_name(ui_file)
            if os.path.exists(ui_file):
                os.unlink(ui_file)

        if os.path.exists("build"):
            shutil.rmtree("build")

class SulinsUninstall(Command):
    user_options = [ ]

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        sulins_dir = os.path.join(get_python_lib(), "sulins")
        if os.path.exists(sulins_dir):
            print("removing: ", sulins_dir)
            shutil.rmtree(sulins_dir)

        conf_dir = "/etc/sulins"
        if os.path.exists(conf_dir):
            print("removing: ", conf_dir)
            shutil.rmtree(conf_dir)
        
        if os.path.exists("/usr/share/applications/sulins.desktop"):
            print("removing: rest of installation", end=' ')
            os.unlink("/usr/share/applications/sulins.desktop")
        os.unlink("/usr/bin/sulins-bin")
        os.unlink("/usr/bin/start-sulins")
        os.unlink("/usr/bin/bindSulins")
        os.unlink("/lib/udev/rules.d/70-sulins.rules")


class I18nInstall(install):
    def run(self):
        install.run(self)
        for lang in I18N_LANGUAGES:
            print("Installing '%s' translations..." % lang)
            os.popen("msgfmt po/%s.po -o po/%s.mo" % (lang, lang))
            if not self.root:
                self.root = "/"
            destpath = os.path.join(self.root, "usr/share/locale/%s/LC_MESSAGES" % lang)
            try:
                os.makedirs(destpath)
            except:
                pass
            shutil.copy("po/%s.mo" % lang, os.path.join(destpath, "%s.mo" % I18N_DOMAIN))

setup(name="sulins",
      version= "3.0.0",
      description="Sulins (Sulin Linux Installer)",
      long_description="Pisi Linux System Installer.",
      license="Latest GNU GPL version",
      author="Pisi Linux Developers",
      author_email="admins@pisilinux.org",
      url="https://github.com/pisilinux/project",
      packages = ['sulins', 'sulins.gui', 'sulins.gui.Ui', 'sulins.storage',\
                  'sulins.storage.devices', 'sulins.storage.formats', 'sulins.storage.library'],
      data_files = [('/etc/sulins', glob.glob("conf/*")),
                    ('/lib/udev/rules.d', ["70-sulins.rules"]),
                    ('/usr/share/applications', ["sulins.desktop"])],
      scripts = ['sulins-bin', 'start-sulins', 'bindSulins'],
      #ext_modules = [Extension('sulins._sysutils',
      #                         sources = ['sulins/_sysutils.c'],
      #                         libraries = ["ext2fs"],
      #                         extra_compile_args = ['-Wall'])],
      cmdclass = {
        'build' : SulinsBuild,
        'clean' : SulinsClean,
        'install': I18nInstall,
        'uninstall': SulinsUninstall
        }
    )
