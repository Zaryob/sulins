#!/usr/bin/python
# -*- coding: utf-8 -*-

import pds.container
import gettext
_ = gettext.translation('sulins', fallback=True).gettext

from PyQt5.Qt import QWidget, pyqtSignal, QGridLayout
import sulins.context as ctx
from sulins.gui import ScreenWidget

class Widget(QWidget, ScreenWidget):
    name = "network"

    def __init__(self):
        QWidget.__init__(self)
        self.layout = QGridLayout(self)
        self.networkConnector = pds.container.PNetworkManager(self)
        self.layout.addWidget(self.networkConnector)

    def shown(self):
        self.networkConnector.startNetworkManager()

    def execute(self):
        self.networkConnector._proc.terminate()
        ctx.mainScreen.disableBack()
        return True
    


