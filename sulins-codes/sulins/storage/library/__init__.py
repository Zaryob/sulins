#!/usr/bin/python
# -*- coding: utf-8 -*-

from sulins.storage import StorageError

class LibraryError(StorageError):
    pass
